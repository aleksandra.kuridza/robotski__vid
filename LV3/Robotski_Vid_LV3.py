import numpy as np
import cv2
import glob
import pickle
import math
import matplotlib.pyplot as plt
import os

#-----------------------FUNKCIJE ZA ODABIR 4 TOČKE NA SLICI--------------------


def CallBackFunc(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        print("Left button of the mouse is clicked - position (", x, ", ",y, ")")
        coordinates.append((x,y));
        
def order_points(pts):
	# initialzie a list of coordinates that will be ordered
	# such that the first entry in the list is the top-left,
	# the second entry is the top-right, the third is the
	# bottom-right, and the fourth is the bottom-left
	rect = np.zeros((4, 2), dtype = "float32")
 
	# the top-left point will have the smallest sum, whereas
	# the bottom-right point will have the largest sum
	
	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]
 
	# now, compute the difference between the points, the
	# top-right point will have the smallest difference,
	# whereas the bottom-left will have the largest difference
	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]
 
	# return the ordered coordinates
	return rect

def four_point_transform(image, pts):
	# obtain a consistent order of the points and unpack them
	# individually
	rect = order_points(pts)
	(tl, tr, br, bl) = rect
 
	# compute the width of the new image, which will be the
	# maximum distance between bottom-right and bottom-left
	# x-coordiates or the top-right and top-left x-coordinates
	widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
	widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
	maxWidth = max(int(widthA), int(widthB))
 
	# compute the height of the new image, which will be the
	# maximum distance between the top-right and bottom-right
	# y-coordinates or the top-left and bottom-left y-coordinates
	heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
	heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
	maxHeight = max(int(heightA), int(heightB))
 
	# now that we have the dimensions of the new image, construct
	# the set of destination points to obtain a "birds eye view",
	# (i.e. top-down view) of the image, again specifying points
	# in the top-left, top-right, bottom-right, and bottom-left
	# order
	dst = np.array([
		[0, 0],
		[maxWidth - 1, 0],
		[maxWidth - 1, maxHeight - 1],
		[0, maxHeight - 1]], dtype = "float32")
 
	# compute the perspective transform matrix and then apply it
	M = cv2.getPerspectiveTransform(rect, dst)
	warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
 
	# return the warped image
	return warped

#------------------KALIBRACIJA KAMERE NA POSTOJEĆIM SLIKAMA---------------------

while (True):
    print("""
Naredbe:
        1 - Kalibriraj kameru
        2 - Uslikaj objekt na milimetarskom papiru
        3 - Odabir rubova uslikanog papira i Houghova transformacija
        4 - Izlaz
        """)
    naredba_broj = input("Unesite broj naredbe: ")

    if naredba_broj == "1":    
        # termination criteria, 28.5 mm duzina/visina jednog kvadratića na sahhovskoj ploci
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 28.5, 0.001)

        # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
        objp = np.zeros((6*8,3), np.float32)
        objp[:,:2] = np.mgrid[0:8,0:6].T.reshape(-1,2)

        # Arrays to store object points and image points from all the images.
        objpoints = [] # 3d point in real world space
        imgpoints = [] # 2d points in image plane.

        images = glob.glob('Sah_ploca/Sahovska_ploca_*.jpg')

        for fname in images:
            img = cv2.imread(fname)
            gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

            # Find the chess board corners
            ret, corners = cv2.findChessboardCorners(gray, (8,6),None)

            # If found, add object points, image points (after refining them)
            if ret == True:
                objpoints.append(objp)
                imgpoints.append(corners)

                # Draw and display the corners
                slika = cv2.drawChessboardCorners(img, (8,6), corners, ret)
                #write_name = 'corners_found'+str(idx)+'.jpg'
                #cv2.imwrite(write_name, img)
                cv2.imshow('img', slika)
                cv2.imwrite('Last_Img_Chessboard.jpg', slika)
                cv2.waitKey(500)        

        cv2.destroyAllWindows()
                
        # Test undistortion on an image
        img = cv2.imread('Sah_ploca/Testna_Slika.jpg')
        img_size = (img.shape[1], img.shape[0])

        # Do camera calibration given object points and image points
        ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, img_size,None,None)

        dst = cv2.undistort(img, mtx, dist, None, mtx)
        cv2.imwrite('Ispravljena__testna_slika.jpg',dst)

        # Save the camera calibration result for later use (we won't worry about rvecs / tvecs)
        dist_pickle = {}
        dist_pickle["mtx"] = mtx #camera matrix 3x3
        dist_pickle["dist"] = dist #distCoeffs matrix 4x1
        dist_pickle["rvecs"] = rvecs #vectors of 3x1 rotation (R) matrix
        dist_pickle["tvecs"] = tvecs #vectors of 3x1 transformation (t) matrix
        pickle.dump( dist_pickle, open( "Parametri_kalibracije.p", "wb" ) )

        #file = open ("Parametri_kalibracije.txt", 'rb')
        #new_file = pickle.load(file)
        #print("parametri", new_file)


        mean_error = 0
        tot_error = 0
        for i in range(len(objpoints)):
            imgpoints2, _ = cv2.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
            error = cv2.norm(imgpoints[i], imgpoints2, cv2.NORM_L2) / len(imgpoints2)
            tot_error += error
            mean_error = tot_error / len(objpoints)
        print ("mean error: ", mean_error)
        print ("total error: ", tot_error)
    #--------------------------------------------------------------------------
    #------------------------SLIKANJE PAPIRA S PREDMETOM NA STOLU--------------
    elif naredba_broj == "2":
        
        if os.path.isfile('./Parametri_kalibracije.p') is not True:
            print("Kamera nije kalibrirana, kalibrirajte kameru odabirom naredbe 1!!")
            continue

        pickle_in = open("Parametri_kalibracije.p", "rb")
        example_dict = pickle.load(pickle_in)
        mtx = example_dict['mtx']
        dist = example_dict['dist']
        
        cam = cv2.VideoCapture(0) #pass 0 to use camera attacted to the computer
        print("Za spremanje slike pritisnite tipku --Enter--")
        while(True):
            ret, frame = cam.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                    
            # Display the resulting frame
            cv2.imshow('frame', gray)

            if cv2.waitKey(1)&0xFF == 13: #ord('\r') is 13 (ascii to unicode code)
                                                  #waitKey(delay in ms)
                cv2.imwrite('Milimetarski_papir.jpg', gray) #save last frame after Enter
                print ("Slika spremljena!")
                        
                image = cv2.imread('Milimetarski_papir.jpg')
                cv2.waitKey()
                break

        mm_paper = cv2.undistort(image, mtx, dist, None, mtx)
        cv2.imwrite('Ispravljena_slika_papir.jpg',mm_paper)

        # When everything done, release the capture
        cam.release()
        cv2.destroyAllWindows()

    #-----------------------------------------------------------------------
    #-----------------------------------------------------------------------
    elif naredba_broj == "3":
        
        if os.path.isfile('./Parametri_kalibracije.p') is not True:
            print("Kamera nije kalibrirana, kalibrirajte kameru odabirom naredbe 1!!")
            continue
        
        pickle_in = open("Parametri_kalibracije.p", "rb")
        example_dict = pickle.load(pickle_in)
        mtx = example_dict['mtx']
        dist = example_dict['dist']

        if os.path.isfile('./Ispravljena_slika_papir.jpg') is not True:
            print("Papir nije uslikan! Uslikajte papir odabirom naredbe 2!!")
            continue
        
        mm_paper = cv2.imread('Ispravljena_slika_papir.jpg')
        
        cv2.waitKey()
        cv2.destroyAllWindows()
        
        #------------------------ODABIR 4 TOČKE NA SLICI MIŠEM---------------------

        print ("Odaberite 4 ruba milimetarskog papira!")
        windowName = 'Odabir 4 ugla papira na slici'
            
        cv2.namedWindow(windowName)
        coordinates = []
        cv2.setMouseCallback(windowName, CallBackFunc)

        cv2.imshow(windowName, mm_paper)

        cv2.waitKey()
        cv2.destroyAllWindows()

        pts = np.array(eval(str(coordinates)), dtype="float32")
        img_4_points = four_point_transform(mm_paper, pts)

        cv2.imshow("Slika_4_ugla_transform", img_4_points)
        cv2.imwrite("Slika_4_ugla_transform.jpg",img_4_points)
        cv2.waitKey()
        cv2.destroyAllWindows()

        #---------------------DETEKCIJA RUBOVA CANNY ALGORITMOM--------------------------------
        edges = cv2.Canny(img_4_points,100,100)
        cv2.imshow('Detekcija rubova Canny metodom', edges)
        cv2.imwrite('Canny.jpg', edges)
        cv2.waitKey()
        cv2.destroyAllWindows()

        #--------------------HOUHOVA TRANSFOMRACIJA, DETEKCIJA LINIJE---------------------

        # This returns an array of r and theta values 
        lines = cv2.HoughLines(edges,1,np.pi/180, 50) 
          
        # The below for loop runs till r and theta values  
        # are in the range of the 2d array 
        for r,theta in lines[0]: 
              
            # Stores the value of cos(theta) in a 
            a = np.cos(theta) 
          
            # Stores the value of sin(theta) in b 
            b = np.sin(theta) 
              
            # x0 stores the value rcos(theta) 
            x0 = a*r 
              
            # y0 stores the value rsin(theta) 
            y0 = b*r 
              
            # x1 stores the rounded off value of (rcos(theta)-1000sin(theta)) 
            x1 = int(x0 + 1000*(-b)) 
              
            # y1 stores the rounded off value of (rsin(theta)+1000cos(theta)) 
            y1 = int(y0 + 1000*(a)) 
          
            # x2 stores the rounded off value of (rcos(theta)+1000sin(theta)) 
            x2 = int(x0 - 1000*(-b)) 
              
            # y2 stores the rounded off value of (rsin(theta)-1000cos(theta)) 
            y2 = int(y0 - 1000*(a)) 
              
            # cv2.line draws a line in img from the point(x1,y1) to (x2,y2). 
            # (0,0,255) denotes the colour of the line to be  
            #drawn. In this case, it is red.  
            cv2.line(img_4_points,(x1,y1), (x2,y2), (0,0,255),2)
            print("%x1, %y1, %x2, %y2", x1, y1, x2, y2)
              
        # All the changes made in the input image are finally 
        # written on a new image houghlines.jpg 
        cv2.imwrite('LinesDetected.jpg', img_4_points)


        #------CRTANJE OKOMITE LINIJE NA PRAVAC (RHO) I IZRAČUN DULJINE I KUTA---------------

        pixels = lines[0][0][0]
        w_img = img_4_points.shape[1] #širina odrezane slike u pikselima
        h_img = img_4_points.shape[0] #visina odrezane slike u piskelima

        scale_w = w_img / 220 #220 mm, stvarna širina označenog dijela papira
        scale_h = h_img / 150 #skaliranje po širini i visini slike zbog veće točnosti

        scale = (scale_h + scale_w)/2
        mms = pixels / scale #pixels je izračunati rho u arrayu lines.

        rads = lines[0][0][1] #rads je izračunati theta u arrayu lines.
        degs = rads * 180 / np.pi #pretvaranje radijana u stupnjeve

        x1 = 0
        y1 = 0
        x2 = int(x1 + pixels * math.cos(rads))
        y2 = int(y1 + pixels * math.sin(rads))

        cv2.line(img_4_points, (x1, y1), (x2, y2), (255, 255, 0), 2)

        font = cv2.FONT_HERSHEY_SIMPLEX
        topLeftCornerOfText = (140, 140)
        fontScale = 0.6
        fontColor = (255,255,0)
        lineType = 2

        cv2.putText(img_4_points,
                    str(str(round(mms, 2)) + " mm, " +
                        str(round(degs, 2)) + " deg"),
                    topLeftCornerOfText,
                    font,
                    fontScale,
                    fontColor,
                    lineType)

        cv2.imshow('Rezultat', img_4_points)
        cv2.imwrite('Rezultat.jpg', img_4_points)
        cv2.waitKey()
        cv2.destroyAllWindows()

    #---------------------------------------------------------------------------
    #---------------------------------------------------------------------------
    elif naredba_broj == "4":
        break

    else:
        print("Krivi odabir naredbe! Odaberite ponovno.")

