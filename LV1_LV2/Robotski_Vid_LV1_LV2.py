import cv2
import numpy as np
from matplotlib import pyplot as plt


while(True):
    
    print("""

Naredbe:
          Broj 1 - Ucitavanje i prikaz slike iz direktorija
          Broj 2 - Obradi sliku uporabom Canny algoritma
          Broj 3 - Izrezi sliku lijevom tipkom misa
          Broj 4 - Spremanje videozapisa s kamere
          Broj 5 - Spremanje slike s kamere
          Broj 6 - Template matching metoda
          Broj 7 - Izlaz
          
          """)

    naredba_broj = input("Unesite broj naredbe: ")
    
#Ucitavanje i prikaz slike iz diirektoriju preko putanje i naziva slike
    if naredba_broj == "1":
        dat = input("Unesite naziv slike iz direktorija ili putanju: ")
        slika = cv2.imread(dat)
        cv2.imshow("Ucitana slika", slika)
        cv2.waitKey()
        cv2.destroyAllWindows()

#Uporaba Canny algoritma za detekciju rubova
    elif naredba_broj == "2":
    
     img = cv2.imread('giraffe.jpg',-1)
     edges = cv2.Canny(img,100,200)
     
     plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
     plt.title('Original Image'), plt.xticks([]), plt.yticks([])
     plt.subplot(122),plt.imshow(edges,cmap = 'gray')
     plt.title('Edge Image'), plt.xticks([]), plt.yticks([])
     plt.show()

#Izrezivanje slike lijevom tipkom miša
        
    elif naredba_broj == "3":
        # Read image
        im = cv2.imread('giraffe.jpg')
        print("Odaberite dio za izrezivanje lijevom tipkom miša i pritisnite Enter!")

        # Select ROI
        r = cv2.selectROI(im)
        
        # Crop image
        imCrop = im[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]

        # Display cropped image
        cv2.imshow("Izrezana slika", imCrop)
        cv2.waitKey(0)

#Spremanje videozapisa s video kamere
    elif naredba_broj == "4":
        cap = cv2.VideoCapture(0)
        out = cv2.VideoWriter('Video_LV1.mp4', -1, 20.0, (640,480))
        print("Za spremanje vidozapisa pritisnite tipku 's'")
        while(cap.isOpened()):
            ret, frame = cap.read()
            if ret==True:
                
                out.write(frame)
                cv2.imshow('frame',frame)
                if cv2.waitKey(1) & 0xFF == ord('s'):
                    break
            else:
                break

        # Release everything if job is finished
        cap.release()
        out.release()
        cv2.destroyAllWindows()


#Spremanje slike s video kamere
    elif naredba_broj == "5":
        
        cam = cv2.VideoCapture(0) #pass 0 to use camera attacted to the computer
        print("Za spremanje slike pritisnite tipku --Enter--")
        while(True):
            ret, frame = cam.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            
            # Display the resulting frame
            cv2.imshow('frame', gray)

            if cv2.waitKey(1)&0xFF == 13: #ord('\r') is 13 (ascii to unicode code)
                                          #waitKey(delay in ms)
                cv2.imwrite('Picture_LV1.jpg', gray) #save last frame after Enter
                print ("Slika spremljena!")
                
                image = cv2.imread('Picture_LV1.jpg')
                cv2.imshow('Slika uslikana kamerom', image)
                cv2.waitKey()
                break

        # When everything done, release the capture
        cam.release()
        cv2.destroyAllWindows()
        

#Metoda podudaranja susjedstva piksela s modelom (Template Matching).
    elif naredba_broj == "6":
        
        img_gray  = cv2.imread('Pic_TemplateMatch.bmp',cv2.COLOR_BGR2GRAY)
        print("Mišem označite jedno slovo (napravite kvadratić) i stisnite Enter!")
        r = cv2.selectROI(img_gray)
        template = img_gray[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
        
        w, h = template.shape[:-1]

        res = cv2.matchTemplate(img_gray,template,cv2.TM_CCOEFF_NORMED)
        threshold = 0.3
        loc = np.where( res >= threshold)
        for pt in zip(*loc[::-1]):
            cv2.rectangle(img_gray, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)

        cv2.imshow('Template matching', img_gray)
        cv2.waitKey()
        cv2.destroyAllWindows()

        
    elif naredba_broj == "7":
        break
    
    else:
        print("Krivi unos broja naredbe! Unesite broj ponovno.")

