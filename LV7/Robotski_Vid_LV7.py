from sklearn.metrics import mean_squared_error
from vtk.util.numpy_support import vtk_to_numpy
import numpy as np
import vtk
import time
import matplotlib.pyplot as plt

referent_bunny = "3D_modeli/bunny.ply"

print("Za unos transformacijskog modela odaberite: 1,2,3,4 ili 5")
inputs = input()

if inputs=="1":
    trans_bunny = "3D_modeli/bunny_t1.ply"
elif inputs=="2":
    trans_bunny = "3D_modeli/bunny_t2.ply"
elif inputs=="3":
    trans_bunny = "3D_modeli/bunny_t3.ply"
elif inputs=="4":
    trans_bunny = "3D_modeli/bunny_t4_parc.ply"
elif inputs=="5":
    trans_bunny = "3D_modeli/bunny_t5_parc.ply"
else :
    print ("Unesite broj ponovo");
print ("Model se obrađuje, pričekajte!");


iterations = [10,50,100,200,500]
landmarks =  [10,50,100,200,500]


reader = vtk.vtkPLYReader()
reader.SetFileName(referent_bunny)
reader.Update()

polydata = reader.GetOutput()
points = polydata.GetPoints()
array = points.GetData()
target_numpy = vtk_to_numpy(array)

targetMapper = vtk.vtkPolyDataMapper()
targetMapper.SetInputConnection(reader.GetOutputPort())
targetActor = vtk.vtkActor()
targetActor.SetMapper(targetMapper)

target = reader.GetOutput()

reader = vtk.vtkPLYReader()
reader.SetFileName(trans_bunny)
reader.Update()

sourceMapper = vtk.vtkPolyDataMapper()
sourceMapper.SetInputConnection(reader.GetOutputPort())
sourceActor = vtk.vtkActor()
sourceActor.SetMapper(sourceMapper)
sourceActor.GetProperty().SetColor(0.0, 1.0, 0.0)

source = reader.GetOutput()



values_il = np.array([])
time_pass = np.array([])
mse = np.array([])

for i in iterations:
    for l in landmarks:
        start_time = time.time()

        #setup ICP transform
        icp = vtk.vtkIterativeClosestPointTransform()
        icp.SetSource(source)
        icp.SetTarget(target)
        icp.GetLandmarkTransform().SetModeToRigidBody()
        icp.SetMaximumNumberOfIterations(i)
        icp.SetMaximumNumberOfLandmarks(l)
        icp.Update()

        #Transform the source points by the ICP solution
        icpTransformFilter = vtk.vtkTransformPolyDataFilter()
        icpTransformFilter.SetInputData(source)
        icpTransformFilter.SetTransform(icp) 
        icpTransformFilter.Update()

        end_time = time.time()

        polydata = icpTransformFilter.GetOutput()
        points = polydata.GetPoints()
        array = points.GetData()
        icp_numpy = vtk_to_numpy(array)

        #izračunaj mse, time_pass i spremi vrijednosti za iteracije i landmark
        mse_t = mean_squared_error(target_numpy, icp_numpy)
        
        values_il = np.append(values_il, "i = "+str(i)+", l = "+str(l))
        mse = np.append(mse, mse_t)
        time_pass = np.append(time_pass, end_time - start_time)
        
        """
        """
        mapper = vtk.vtkPolyDataMapper()
        mapper.SetInputConnection(icpTransformFilter.GetOutputPort())
        solutionActor = vtk.vtkActor()
        solutionActor.SetMapper(mapper)

#plotaj grafove za MSE i Vrijeme izvršavanja za svaku kombinaciju iteracija i landmarkova
plt.figure()
plt.bar(values_il, mse, color='r')
plt.ylabel("Srednja kvadratna pogreska")
plt.xlabel("Broj iteracija i max broj korespodencija")
plt.title(trans_bunny)
plt.xticks(rotation=90)
plt.tight_layout()
plt.savefig(trans_bunny+'_1'+'.png')

plt.figure()
plt.bar(values_il, time_pass, color='r')
plt.ylabel("Vrijeme [s]")
plt.xlabel("Broj iteracija i max broj korespodencija")
plt.title(trans_bunny)
plt.xticks(rotation=90)
plt.tight_layout()
plt.savefig(trans_bunny+'_2'+'.png')
plt.show()


#unos optimalnih parametara dobivenih iz iščitavanja grafova
print("Promatrajući graf unesite najbolji broj iteracija: ")
it = input()
it=int(it)
print("Promatrajući graf unesite najbolji broj landmarkova: ")
lan=  input()
lan= int(lan)

#izračun vremena izvršavanja ICP algoritma za optimalne parametre
iteration_index=iterations.index(it)
landmark_index=landmarks.index(lan)
time_passed=time_pass[len(iterations)*iteration_index+landmark_index]
print ('Vrijeme izvršavanja: %f' %time_passed)

# SETUP ICP TRANSFORM
icp = vtk.vtkIterativeClosestPointTransform()
icp.SetSource(source)
icp.SetTarget(target)
icp.GetLandmarkTransform().SetModeToRigidBody()
icp.SetMaximumNumberOfIterations(it)
icp.SetMaximumNumberOfLandmarks(lan)
icp.Update()

#TRANSFORM THE SOURCE POINTS BY THE ICP SOLUTION
icpTransformFilter = vtk.vtkTransformPolyDataFilter()
icpTransformFilter.SetInputData(source)
icpTransformFilter.SetTransform(icp)
icpTransformFilter.Update()

#VISUALIZE
solutionMapper  = vtk.vtkPolyDataMapper()
solutionMapper .SetInputConnection(icpTransformFilter.GetOutputPort())

solutionActor = vtk.vtkActor()
solutionActor.SetMapper(solutionMapper )
solutionActor.GetProperty().SetColor(0.0, 1.0, 0.0)

#ADD TEXT ACTOR ON RENDERER WINDOW
text_actor2 = vtk.vtkTextActor()
text_actor2.SetInput(trans_bunny +", I- " + str(it) + ", L- " + str(lan)
                     + ", Time_passed- " + str(time_passed))
text_actor2.SetPosition(25,600)
text_actor2.GetTextProperty().SetFontSize(20)




#renderer
renderer = vtk.vtkRenderer()

#renderer window
renderWindow = vtk.vtkRenderWindow()
renderWindow.SetSize(650, 650)
renderWindow.AddRenderer(renderer)

#interactor
renderWindowInteractor = vtk.vtkRenderWindowInteractor()
renderWindowInteractor.SetRenderWindow(renderWindow)

#Add actor to the scene
renderer.AddActor(targetActor)
renderer.AddActor(solutionActor)
renderer.AddActor2D(text_actor2)

#Render and interact
renderWindow.Render()
renderWindow.SetWindowName(trans_bunny)
renderWindowInteractor.Start()

