import cv2
import numpy as np
from os import listdir

#function from Listing6_1
def ReadKinectPics():
    KinectPics = []
    RGBPics = []
    pics_path = "./KinectPics/"
    files = [f for f in listdir(pics_path)]
    files = np.sort(files)

    for i in range(0, len(files), 2):
        D = np.loadtxt(pics_path+files[i])
        RGB = cv2.imread(pics_path+files[i+1])
        RGBPics.append(RGB)
        pts3D = []
        dmin = 2047
        dmax = 0
        for v in range(D.shape[0]):
            for u in range(D.shape[1]):
                if D[v, u] == 2047:
                    D[v, u] = -1
                elif D[v, u] < dmin:
                    dmin = D[v, u]
                elif D[v, u] > dmax:
                    dmax = D[v, u]
                if D[v, u] != -1:
                    pts3D.append([v, u, D[v, u]])
        D = (D-dmin)*254/(dmax-dmin)+1
        D[D < 0] = 0
        D = np.uint8(D)
        KinectPics.append([np.array(pts3D), D])

    return KinectPics, RGBPics


def get_plane_from_points():  #korak 2
    rp_indexes = np.random.choice(points_len, 3, replace=False) #Generates a random sample (i, j, k) from a given 1-D array (uzmi 3 random indexa iz ukupne velicine niza)
    random_points = points[rp_indexes] #nasumicno odaberi 3 tocke (u,v,d) iz niza 
    #print (random_points)  # 3x(1x3) matrix
    
    u = random_points[:, 0] #take just first row in random_points matrix
    v = random_points[:, 1]
    d = random_points[:, 2]
    
    mat = np.array([[u[0], v[0], 1], #matrica 3x3 iz jednadžbe 2
                    [u[1], v[1], 1],
                    [u[2], v[2], 1]])

    return np.matmul(np.linalg.inv(mat), d.transpose()) #[a,b,c]
    

def get_points_on_plane(R, threshold): #korak 3
    a = R[0] #a iz jednadžbe 2
    b = R[1] #b iz jednadzbe 2
    c = R[2] #c iz jednadžbe 2
    
    u = points[:, 0] #first row - 3xn matrix (points- sve tocke iz oblaka)
    v = points[:, 1] #second row -3xn matrix
    d = points[:, 2] #matrix 3xn

    tmp_points = np.abs(d - (a * u + b * v + c)) #odredi koje točke leže na ravnini
    pp_indexes = np.where(tmp_points < threshold)[0]
    return points[pp_indexes]




KPics, RGBPics = ReadKinectPics()
img_counter = 0

for image in range(0, 9):
    
    points = np.int32(KPics[image][0])  #points from image cloud (3x(number_of_points) matrix)
    
    depth_map = KPics[image][1] #points from .txt file for KPics
    
    depth_map = cv2.cvtColor(depth_map, cv2.COLOR_GRAY2BGR)

    points_len = points.shape[0]
    ponavljanja = 950

    R_dominant = np.array([])
    T_dominant = np.array([])

    for i in range(ponavljanja):
        R = [] #ravnina --> korak 1 au+bv+c=d, R ravnina s parametrima [a,b,c]

        while(R == []):
            R = get_plane_from_points() #ravnina na slici (onoliko ravnina koliko je ponavljanja)

        tmp_points = get_points_on_plane(R, 3) # (plane, treshold)
        
        if tmp_points.shape[0] > T_dominant.shape[0]: #korak 4
            #ako je broj točaka veći od prethodno izračunatog
            T_dominant = tmp_points #korak 5
            #postavi novi broj točaka za dominantni broj
            R_dominant = R    #korak 6
            #postavi novu izračunatu ravninu za dominantnu (ima najviše točaka koje na njoj leže)

    depth_map_indexes = T_dominant[:,:2] #spremi indexe točaka koje se nalaze na dominantnoj ravnini
    print (R_dominant)
    depth_map[depth_map_indexes[:, 0], depth_map_indexes[:, 1]] = [255, 0, 0] #BGR colors, prikaži dominantnu ravninu plavom bojom

    rgb_img = RGBPics[image]
    image = np.hstack((rgb_img, depth_map))
    #cv2.imshow("Rezultat s dominantnom ravninom: '%.3f', '%.3f', '%.3f'" % (R_dominant[0],R_dominant[1],R_dominant[2]), image)
    img_name = ("Rezultati/Rezultat_Slika_{} - '%.3f','%.3f','%.3f'.jpg" %(R_dominant[0],R_dominant[1],R_dominant[2])).format(img_counter)
    cv2.imwrite(img_name, image)
    img_counter +=1 
    cv2.waitKey()
    cv2.destroyAllWindows()
    