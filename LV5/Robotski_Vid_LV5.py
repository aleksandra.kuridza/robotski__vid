import cv2
import numpy as np
import pickle
import plotly.graph_objs as go
import matplotlib.pyplot as plt
from plotly.offline import init_notebook_mode, iplot


init_notebook_mode(connected=True)


#Funkcija iz Listing5.1 //IZ C++ u Python preveo kolega Toni Birka.
def Convert2DPointsTo3DPoints(pts2D_L, pts2D_R, E, P):
    #Find the inverse of P (projection matrix)
    P = np.matrix(P)

    #Determine the singular value decomposition (svd) of E (essential matrix)
    D, U, Vt = cv2.SVDecomp(E)

    #Define W
    W = [[0, -1, 0],
         [1, 0, 0],
         [0, 0, 1]]
    W = np.float32(W)

    A = np.matrix(U.dot(W).dot(Vt))

    b = np.zeros([3, 1], np.float)
    b[:, 0] = U[:, 2]

    Ainv_b = A.I.dot(b)

    #**** Helper matrices ****
    Lpi = np.zeros([3, 1], np.float)
    Rpi = np.zeros([3, 1], np.float)
    ARpi = np.zeros([3, 1], np.float)

    S = np.zeros([2, 1], np.float)

    X = np.zeros([2, 2], np.float)
    x1 = np.zeros([1, 1], np.float)
    x2 = np.zeros([1, 1], np.float)
    x4 = np.zeros([1, 1], np.float)
    Y = np.zeros([2, 1], np.float)

    y1, y2 = np.zeros([1, 1], np.float), np.zeros([1, 1], np.float)

    Lm, Rm = np.zeros([3, 1], np.float), np.zeros([3, 1], np.float)

    #Iteratively convert 2D point pairs to 3D points
    pts3D = []

    for i in range(len(pts2D_L)):
        Lm[0, 0] = pts2D_L[i][0]
        Lm[1, 0] = pts2D_L[i][1]
        Lm[2, 0] = 1

        Rm[0, 0] = pts2D_R[i][0]
        Rm[1, 0] = pts2D_R[i][1]
        Rm[2, 0] = 1

        Lpi = P.I.dot(Lm)
        Rpi = P.I.dot(Rm)

        # Init X table
        ARpi = A.I.dot(Rpi)
        x1 = Lpi.T.dot(Lpi)
        x2 = Lpi.T.dot(ARpi)
        x4 = ARpi.T.dot(ARpi)

        X[0, 0] = -x1[0, 0]
        X[0, 1] = x2[0, 0]
        X[1, 0] = x2[0, 0]
        X[1, 1] = -x4[0, 0]

        # Init Y table
        y1 = Lpi.T.dot(Ainv_b)
        y2 = ARpi.T.dot(Ainv_b)
        Y[0, 0] = -y1[0, 0]
        Y[1, 0] = y2[0, 0]

        cv2.solve(X, Y, S)

        s = S[0, 0]
        t = S[1, 0]

        Lpi = s*Lpi
        ARpi = t*ARpi

        pts3D.append(np.array(Lpi + ARpi - Ainv_b/2).T)

    return np.array(pts3D).reshape(len(pts3D), 3)


while(True):
    
    print("""
Naredbe:
          1- Uslikaj 2 slike objekta za rekonstrukciju (slikanje pritiskom na tipku SPACE)
          2- Učitaj postojeće slike i napravi trodimenzionalnu rekonstrukciju scene
          3- Izlaz
          """)

    broj_naredbe = input()
 
    #---------SLIKANJE DVIJE SLIKE OBJEKTA NA SCENI POD RAZLIČITIM KUTEM------------
    if broj_naredbe == "1":
        
        cam = cv2.VideoCapture(0)
        cv2.namedWindow("test")
        img_counter = 0
        
        print ("Stisnite Space za slikanje objekta")
        
        while img_counter < 2:
            ret, frame = cam.read()
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            cv2.imshow("test", frame)
            
            if not ret:
                break
            k = cv2.waitKey(1)
        
            if k%256 == 27:
                # ESC pressed
                print("Stisnite ESC za izlaz...")
                break
            
            elif k%256 == 32:
                # SPACE pressed
                img_name = "Objekt_slika_{}.jpg".format(img_counter)
                cv2.imwrite(img_name, frame)
                print("{} spremljena!".format(img_name))
                img_counter += 1
        
        cam.release()
        cv2.destroyAllWindows()    







#------------------UČITAJ POSTOJEĆE SLIKE---------------------
    elif broj_naredbe == "2":

        img1 = cv2.imread('Objekt_slika_0.jpg', 0)    
        img2 = cv2.imread('Objekt_slika_1.jpg', 0)


        cv2.imshow("Slika 1", img1)
        cv2.imshow("Slika 2", img2)
        cv2.waitKey()
        cv2.destroyAllWindows()
        
#-----------------NA SLIKAMA NAĐI SIFT ZNAČAJKE-----------------

        sift = cv2.xfeatures2d.SIFT_create()

        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img1, None)
        kp2, des2 = sift.detectAndCompute(img2, None)

        # FLANN parameters
        flann = cv2.FlannBasedMatcher()
        matches = flann.knnMatch(des1, des2, k=2)

        good = []
        pts1 = []
        pts2 = []
        
        # ratio test as per Lowe's paper
        for m, n in matches:
            if m.distance < 0.75*n.distance:
                good.append(m)
                pts1.append(kp1[m.queryIdx].pt) #index za KP m na prvoj slici
                pts2.append(kp2[m.trainIdx].pt) #index za KP m na drugoj slici


        pts1 = np.int32(pts1)
        pts2 = np.int32(pts2)

    	#Fundamental Matrix
        F, mask = cv2.findFundamentalMat(pts1, pts2, cv2.FM_RANSAC)
        
        
        #------------PRIKAŽI SVE SPARENE ZNAČAJKE NA OBJE SLIKE------------
        img3 = cv2.drawMatches(img1, kp1, img2, kp2, good, None, flags=2)
        cv2.imshow("All matches from both the images", img3)
        cv2.imwrite("All_matches.jpg",img3)
        cv2.waitKey()
        cv2.destroyAllWindows()
        
        
        # # We select only inlier points
        pts1 = pts1[mask.ravel() == 1]
        pts2 = pts2[mask.ravel() == 1]

        
        #--------------UZIMAMO SAMO ONE DOBRO SPARENE ZNAČAJKE-------------
        best = []
        for m in good:
            best_pts1 = np.int32(kp1[m.queryIdx].pt) #index za KP m na prvoj slici
            best_pts2 = np.int32(kp2[m.trainIdx].pt) #index za KP m na drugoj slici
            for i in range(len(pts1)):
                if (np.array_equal(best_pts1, pts1[i]) and np.array_equal(best_pts2, pts2[i])):
                    best.append(m)
                   
                       
        #-----------prilaži samo dobro sparene značajke--------------------
        img4 = cv2.drawMatches(img1, kp1, img2, kp2, best, None, flags=2)
        cv2.imshow("Best matches from both the images", img4)
        cv2.imwrite("Best_matches.jpg",img4)
        cv2.waitKey()
        cv2.destroyAllWindows()
        
        
        #----------------------KALIBRACIJA KAMERE------------------------
        pickle_in = open("Parametri_kalibracije.p", "rb")
        example_dict = pickle.load(pickle_in)
        mtx = example_dict['mtx'] #camera matrix 3x3
        rvecs= example_dict['rvecs']
        tvecs= example_dict['tvecs']
        
        #Essential matrix
        E, mask = cv2.findEssentialMat(pts1, pts2, mtx, cv2.RANSAC)
        
        """
        print (E)
        # projection matrix P = multiply (mtx, R|t)
        rotation_mat = np.zeros(shape=(3, 3))
        R = cv2.Rodrigues(rvecs[0], rotation_mat)[0]
        #print (R)
        R_t=np.hstack((R,tvecs[0]))
        #print (R_t)
        P = np.matmul(mtx,R)
        Ess= np.matrix.transpose(P)
        Ess= np.multiply(Ess,F)
        Ess= np.multiply(Ess,P)
        print(Ess)
        
        """
        
        pts3D = Convert2DPointsTo3DPoints(pts1, pts2, E, mtx)

        x = pts3D[:, 0]
        y = pts3D[:, 1]
        z = pts3D[:, 2]
        
        
        #----------------SPREMI KOORDINATE U .TXT FILE
        with open('Save_coordinates.txt', 'w') as fh:
            for i in range(len(pts3D)):
                fh.write('{} {} {}\n'.format(x[i],y[i],z[i]))
        
        
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_trisurf(x, y, z)
        
        fig.show()
        
        

    elif broj_naredbe == "3":
        break
    
    else:
        print("Krivi odabir naredbe! Odaberite ponovno.")
