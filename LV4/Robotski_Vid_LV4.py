from __future__ import print_function
import cv2
import numpy as np
import matplotlib.pyplot as plt

#load images
img1 = cv2.imread('slika_0.jpg', 0)    
img2 = cv2.imread('slika_2.jpg', 0) 

#resize images
img1 = cv2.resize(img1, (0,0), fx=0.4, fy=0.4) 
img2 = cv2.resize(img2, (0,0), fx=0.4, fy=0.4)

#display images

#cv2.imshow("Slika 1", img1)
#cv2.imshow("Slika 2", img2)
cv2.waitKey()
cv2.destroyAllWindows()

# Select ROI
r = cv2.selectROI(img1)
     
# Crop image
img1 = img1[int(r[1]):int(r[1]+r[3]), int(r[0]):int(r[0]+r[2])]
 
# Display cropped image
cv2.imshow("Cropped image", img1)
cv2.imwrite('Rezultati/Cropped_image.jpg',img1)
cv2.waitKey()
cv2.destroyAllWindows()

MIN_MATCH_COUNT = 10

sift = cv2.xfeatures2d.SIFT_create()
# find the keypoints and descriptors with SIFT
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)


#spremi znacajke na croppanoj slici
img_small = cv2.drawKeypoints(img1, kp1, None)
cv2.imwrite('Rezultati/Znacajke_mala_slika.jpg',img_small)
cv2.waitKey()
cv2.destroyAllWindows()

#spremi znacajke na cijeloj drugoj slici
img_all = cv2.drawKeypoints(img2, kp2, None)
cv2.imwrite('Rezultati/Znacajke_velika_slika.jpg',img_all)
cv2.waitKey()
cv2.destroyAllWindows()



FLANN_INDEX_KDTREE = 0
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks = 50)

flann = cv2.FlannBasedMatcher(index_params, search_params)

matches = flann.knnMatch(des1,des2,k=2)

# store all the good matches as per Lowe's ratio test.
good = []
for m,n in matches:
    if m.distance < 0.75*n.distance:
        good.append(m)
        
if len(good)>MIN_MATCH_COUNT:
    src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
    dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)

    M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
    matchesMask = mask.ravel().tolist()

    h,w = img1.shape
    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
    dst = cv2.perspectiveTransform(pts,M)

    img2 = cv2.polylines(img2,[np.int32(dst)],True,255,3, cv2.LINE_AA)

else:
    print ("Not enough matches are found - %d/%d" % (len(good),MIN_MATCH_COUNT))
    matchesMask = None
    
draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                   singlePointColor = None,
                   matchesMask = matchesMask, # draw only inliers
                   flags = 2)

img3 = cv2.drawMatches(img1,kp1,img2,kp2,good,None,**draw_params)

plt.imshow(img3, 'gray'),plt.show()

cv2.imwrite('Rezultati/Slika_Rezultat.jpg',img3)
